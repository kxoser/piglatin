package piglatin;

import static org.junit.Assert.*;

import org.junit.Test;

public class TranslatorTest {

	@Test
	public void inputPhraseShouldBeTranslated() {
		String inputPhrase = "Hello World";
		Translator translator = new Translator(inputPhrase);
		assertEquals("Hello World", translator.getPhrase());	
	}
	
	@Test
	public void translationEmptyPhrase() {
		String inputPhrase = "";
		Translator translator = new Translator(inputPhrase);
		assertEquals(Translator.NIL, translator.translate());	
	}
	
	@Test
	public void translationPhraseStartingWithAEndingY() {
		String inputPhrase = "any";
		Translator translator = new Translator(inputPhrase);
		assertEquals("anynay", translator.translate());	
	}
	
	@Test
	public void translationPhraseStartingWithUEndingY() {
		String inputPhrase = "utility";
		Translator translator = new Translator(inputPhrase);
		assertEquals("utilitynay", translator.translate());	
	}
	
	@Test
	public void translationPhraseStartingWithVowelEndingWithVowel() {
		String inputPhrase = "apple";
		Translator translator = new Translator(inputPhrase);
		assertEquals("appleyay", translator.translate());	
	}
	
	@Test
	public void translationPhraseStartingWithVowelEndingWithConsonant() {
		String inputPhrase = "ask";
		Translator translator = new Translator(inputPhrase);
		assertEquals("askay", translator.translate());	
	}
	
	@Test
	public void translationPhraseStartingWithConsonant() {
		String inputPhrase = "hello";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay", translator.translate());	
	}
	
	@Test
	public void translationPhraseStartingWithMoreConsonants() {
		String inputPhrase = "known";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ownknay", translator.translate());	
	}
	
	@Test
	public void translationPhraseWithMoreWordsAndWhiteSpaces() {
		String inputPhrase = "hello world";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay orldway", translator.translate());	
	}
	
	@Test
	public void translationPhraseWithMoreCompositeWords() {
		String inputPhrase = "well-being";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellway-eingbay", translator.translate());	
	}
	
	@Test
	public void translationPhraseWithCompositeWordsAndWhiteSpaces() {
		String inputPhrase = "hello well-being";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay ellway-eingbay", translator.translate());	
	}
	
	@Test
	public void translationPhraseWithDot() {
		String inputPhrase = "hello world.";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay orldway.", translator.translate());	
	}
	
	@Test
	public void translationPhraseWithComma() {
		String inputPhrase = "hello world,";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay orldway,", translator.translate());	
	}
	
	@Test
	public void translationPhraseWithMiddleComma() {
		String inputPhrase = "hello, world";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay, orldway", translator.translate());	
	}
	
	@Test
	public void translationPhraseWithSemiColon() {
		String inputPhrase = "hello world;";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay orldway;", translator.translate());	
	}
	
	@Test
	public void translationPhraseWithColon() {
		String inputPhrase = "hello world:";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay orldway:", translator.translate());	
	}
	
	@Test
	public void translationPhraseWithQuestionMark() {
		String inputPhrase = "hello world?";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay orldway?", translator.translate());	
	}
	
	@Test
	public void translationPhraseWithExclamationMark() {
		String inputPhrase = "hello world!";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay orldway!", translator.translate());	
	}
	
	@Test
	public void translationPhraseWithApostrophe() {
		String inputPhrase = "bryan's bow";
		Translator translator = new Translator(inputPhrase);
		assertEquals("anbryay's owbay", translator.translate());	
	}
	
	@Test
	public void translationPhraseStartingWithVowelContainingApostrophe() {
		String inputPhrase = "amy's bow";
		Translator translator = new Translator(inputPhrase);
		assertEquals("amynay's owbay", translator.translate());	
	}
	
	@Test
	public void translationPhraseWithRoundParenthesis() {
		String inputPhrase = "(hello world)";
		Translator translator = new Translator(inputPhrase);
		assertEquals("(ellohay orldway)", translator.translate());	
	}

}
