package piglatin;

public class Translator {

	public static final String NIL = "nil";
	
	private String phrase;
	
	public Translator(String inputPhrase) {
		phrase = inputPhrase;
	}

	public String getPhrase() {
		return phrase;
	}

	public String translate() {
		
		if(!phrase.equals("")) {
			return traslatePhraseWithMoreWords(" ");		
		}
		
		return NIL;
	
	}

	private String getPunctuation(String phraseTraslated) {
		if(phraseTraslated.indexOf('.') != -1) {return ".";}
		if(phraseTraslated.indexOf(',') != -1) {return ",";}
		if(phraseTraslated.indexOf(';') != -1) {return ";";}
		if(phraseTraslated.indexOf(':') != -1) {return ":";}
		if(phraseTraslated.indexOf('?') != -1) {return "?";}
		if(phraseTraslated.indexOf('!') != -1) {return "!";}

		return "";
	}
	
	private boolean checkExistingParenthesis() {
		return (phrase.indexOf('(') != -1 || phrase.indexOf(')') != -1);
	}
	
	private boolean checkExistingApostrophe() {
		return (phrase.indexOf('\'') != -1);
	}
	
	private boolean startWithVowel(String newPhrase) {
		return(newPhrase.startsWith("a") || newPhrase.startsWith("e") || newPhrase.startsWith("i") || newPhrase.startsWith("o") || newPhrase.startsWith("u"));	
	}
	
	private String traslatePhraseWithMoreWords(String separator) {
		
		String phraseWithoutPunctuations;
		String suffix = "";
		
		if(checkExistingParenthesis()) {
		    phraseWithoutPunctuations = phrase.replace("(","");
			phraseWithoutPunctuations = phraseWithoutPunctuations.replace(")","");
			
		}else if(checkExistingApostrophe()){
			suffix = phrase.substring(phrase.indexOf("'"), phrase.indexOf(" "));
			phraseWithoutPunctuations = phrase.replace(suffix,"");
		}else {
		    phraseWithoutPunctuations = phrase.replace(getPunctuation(phrase),"");
		}

		String[] phraseSplittedBySeparatorWithPunctuations = phrase.split(separator);
		String[] phraseSplittedBySeparator = phraseWithoutPunctuations.split(separator);
		int counterOfWords = phraseSplittedBySeparator.length;
		
		StringBuilder phraseTraslated = new StringBuilder();
		for(int j = 0; j != counterOfWords; j++) {
		
			if(j > 0) {phraseTraslated = phraseTraslated.append(suffix + separator);}
			
			if(checkIsCompositeWord(phraseSplittedBySeparator[j])) {
				phraseTraslated = translateSingleCompositeWord(phraseTraslated,suffix,phraseSplittedBySeparator[j]);
				phraseTraslated = phraseTraslated.append(getPunctuation(phraseSplittedBySeparatorWithPunctuations[j]));
			}else {
				phraseTraslated = translateSingleWord(phraseTraslated, phraseSplittedBySeparator[j]);
				phraseTraslated = phraseTraslated.append(getPunctuation(phraseSplittedBySeparatorWithPunctuations[j]));
			}

		}
	
		if(checkExistingParenthesis()) {
			return "(" + phraseTraslated.toString() + ")";
		}
		return phraseTraslated.toString();
	}

	private String makeTheWordStartWithVowel(String word) {
		int i = 0;
		StringBuilder addChar = new StringBuilder();
		for(i = 0; !startWithVowel(word.substring(i)); i++) {
			addChar = addChar.append(word.charAt(i));		
		}
		return word.substring(i) + addChar;
	}

	private boolean endsWithVowel() {
		return(phrase.endsWith("a") || phrase.endsWith("e") || phrase.endsWith("i") || phrase.endsWith("o") || phrase.endsWith("u"));	
	}
	
	private boolean checkIsCompositeWord(String word) {
		return (word.indexOf('-') != -1);
	}
	
	private StringBuilder translateSingleWord(StringBuilder phraseTraslated, String word) {
		
		if(startWithVowel(word)) {
			if(word.endsWith("y")) {
				phraseTraslated = phraseTraslated.append(word + "nay");
			} else if(endsWithVowel()) {
				phraseTraslated = phraseTraslated.append(word + "yay");
			} else if(!endsWithVowel()) {
				phraseTraslated = phraseTraslated.append(word + "ay");
			}
		}else {
			String wordStartingWithVowel = makeTheWordStartWithVowel(word);
			phraseTraslated = phraseTraslated.append( wordStartingWithVowel + "ay");
		}
		
		return phraseTraslated;
	}
	
	private StringBuilder translateSingleCompositeWord(StringBuilder phraseTraslated,String suffix,String word) {
		String[] phraseComposed = word.split("-");
		int counterOfWordsOfPhraseComposed = phraseComposed.length;
		
		for(int k = 0; k != counterOfWordsOfPhraseComposed; k++) {
			
			if(k > 0) {phraseTraslated = phraseTraslated.append(suffix + "-");}
			
			phraseTraslated = translateSingleWord(phraseTraslated, phraseComposed[k]);
		}
		
		return phraseTraslated;
	}

}
